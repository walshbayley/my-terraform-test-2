terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
      version = "~>2.0"
    }
  }
}


variable "sub_id" {
  type = string
}

variable "ten_id" {
  type = string
}

variable "cli_id" {
  type = string
}

variable "cli_secret" {
  type = string
}


provider "azurerm" {
  features {}

  subscription_id   = var.sub_id
  tenant_id         = var.ten_id
  client_id         = var.cli_id
  client_secret     = var.cli_secret
}

resource "azurerm_resource_group" "main" {
  name     = "flux-test-resources"
  location = "East US"
}

resource "azurerm_virtual_network" "main" {
  name                = "flux-test-network"
  address_space       = ["10.2.0.0/16"]
  location            = azurerm_resource_group.main.location
  resource_group_name = azurerm_resource_group.main.name
}

resource "azurerm_subnet" "internal" {
  name                 = "internal"
  resource_group_name  = azurerm_resource_group.main.name
  virtual_network_name = azurerm_virtual_network.main.name
  address_prefixes     = ["10.2.2.0/24"]
}

resource "azurerm_network_interface" "main" {
  name                = "flux-test-nic"
  resource_group_name = azurerm_resource_group.main.name
  location            = azurerm_resource_group.main.location

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.internal.id
    private_ip_address_allocation = "Dynamic"
  }
}

resource "azurerm_linux_virtual_machine" "main" {
  name                = "flux-test-vm"
  resource_group_name = azurerm_resource_group.main.name
  location            = azurerm_resource_group.main.location
  size                = "Standard_F2"
  admin_username      = "adminuser"
  admin_password      = "TesTing123$$%"
  disable_password_authentication = "false"
  network_interface_ids = [
    azurerm_network_interface.main.id,
  ]
  
  source_image_reference {
    publisher = "Canonical"
    offer     = "0001-com-ubuntu-server-jammy"
    sku       = "22_04-lts"
    version   = "latest"
  }

  os_disk {
    storage_account_type = "Standard_LRS"
    caching              = "ReadWrite"
  }
}