terraform {
  required_version = ">= 0.12.26"
}

variable "subject" {
   type = string
   default = "tfctl-rc"
   description = "Subject to hello"
}

resource "random_id" "server" {
  keepers = {
    version = 4
  }
  byte_length = 20
}

output "hello_world" {
  value = "hey hey ya, ${var.subject}!"
}

output "server_id" {
  value = random_id.server.dec
}
